# Set modelname and path to Pflow preprocessing config file
model_name: DL1r-PFlow_new-taggers-stats-22M
preprocess_config: examples/PFlow-Preprocessing.yaml

# Add here a pretrained model to start with.
# Leave empty for a fresh start
model_file:

# Add training file
train_file: /work/ws/nemo/fr_af1100-Training-Simulations-0/preprocessed/PFlow-hybrid-preprocessed_shuffled.h5

# Add validation files
# ttbar val
validation_file: /work/ws/nemo/fr_af1100-Training-Simulations-0/hybrids/MC16d_hybrid_odd_100_PFlow-no_pTcuts-file_0.h5

# zprime val
add_validation_file: /work/ws/nemo/fr_af1100-Training-Simulations-0/hybrids/MC16d_hybrid-ext_odd_0_PFlow-no_pTcuts-file_0.h5

ttbar_test_files:
    ttbar_r21:
        Path: /work/ws/nemo/fr_af1100-Training-Simulations-0/hybrids/MC16d_hybrid_odd_100_PFlow-no_pTcuts-file_1.h5
        data_set_name: "ttbar_r21"

    ttbar_r22:
        Path: /work/ws/nemo/fr_af1100-Training-Simulations-0/hybrids_r22/MC16d_hybrid-r22_odd_100_PFlow-no_pTcuts-file_1.h5
        data_set_name: "ttbar_r22"

zpext_test_files:
    zpext_r21:
        Path: /work/ws/nemo/fr_af1100-Training-Simulations-0/hybrids/MC16d_hybrid-ext_odd_0_PFlow-no_pTcuts-file_1.h5
        data_set_name: "zpext_r21"

    zpext_r22:
        Path: /work/ws/nemo/fr_af1100-Training-Simulations-0/hybrids_r22/MC16d_hybrid-r22-ext_odd_0_PFlow-no_pTcuts-file_1.h5
        data_set_name: "zpext_r22"

# Path to Variable dict used in preprocessing
var_dict: umami/configs/DL1r_Variables.yaml

exclude: null

NN_structure:
    # Decide, which tagger is used
    tagger: "dl1"

    # NN Training parameters
    lr: 0.001
    batch_size: 15000
    epochs: 200

    # Number of jets used for training
    # To use all: Fill nothing
    nJets_train:

    # Dropout rate. If = 0, dropout is disabled
    dropout: 0

    # Define which classes are used for training
    # These are defined in the global_config
    class_labels: ["ujets", "cjets", "bjets"]

    # Main class which is to be tagged
    main_class: "bjets"

    # Decide if Batch Normalisation is used
    Batch_Normalisation: False

    # Nodes per dense layer. Starting with first dense layer.
    dense_sizes: [256, 128, 60, 48, 36, 24, 12, 6]

    # Activations of the layers. Starting with first dense layer.
    activations: ["relu", "relu", "relu", "relu", "relu", "relu", "relu", "relu"]

    # Options for the Learning Rate reducer
    LRR: True

    # Option if you want to use sample weights for training
    use_sample_weights: False

# Plotting settings for training metrics plots
Validation_metrics_settings:
    # Define which taggers should also be plotted
    taggers_from_file: ["DL1r"]

    # Enable/Disable atlas tag
    UseAtlasTag: True

    # fc_value and WP_b are autmoatically added to the plot label
    AtlasTag: "Internal Simulation"
    SecondTag: "\n$\\sqrt{s}=13$ TeV, PFlow jets"

    # Set the datatype of the plots
    plot_datatype: "pdf"

# Eval parameters for validation evaluation while training
Eval_parameters_validation:
    # Number of jets used for validation
    n_jets: 3e5

    # Define taggers that are used for comparison in evaluate_model
    # This can be a list or a string for only one tagger
    tagger: ["rnnip", "DL1r"]

    # Define fc values for the taggers
    frac_values_comp: {
        "rnnip": {
            "cjets": 0.08,
            "ujets": 0.92,
        },
        "DL1r": {
            "cjets": 0.018,
            "ujets": 0.982,
        },
    }

    # Charm fraction value used for evaluation of the trained model
    frac_values: {
        "cjets": 0.018,
        "ujets": 0.982,
    }

    # Cuts which are applied to the different datasets used for evaluation
    variable_cuts:
        validation_file:
            - pt_btagJes:
                operator: "<="
                condition: 250000

        add_validation_file:
            - pt_btagJes:
                operator: ">"
                condition: 250000

        ttbar_r21:
            - pt_btagJes:
                operator: "<="
                condition: 250000

        ttbar_r22:
            - pt_btagJes:
                operator: "<="
                condition: 250000

        zpext_r21:
            - pt_btagJes:
                operator: ">"
                condition: 250000

        zpext_r22:
            - pt_btagJes:
                operator: ">"
                condition: 250000

    # A list to add available variables to the evaluation files
    add_variables_eval: ["actualInteractionsPerCrossing"]

    # Working point used in the evaluation
    WP: 0.77

    # some properties for the feature importance explanation with SHAPley
    shapley:
        # Over how many full sets of features it should calculate over.
        # Corresponds to the dots in the beeswarm plot.
        # 200 takes like 10-15 min for DL1r on a 32 core-cpu
        feature_sets: 200

        # defines which of the model outputs (flavor) you want to explain
        # [tau,b,c,u] := [3, 2, 1, 0]
        model_output: 2

        # You can also choose if you want to plot the magnitude of feature
        # importance for all output nodes (flavors) in another plot. This
        # will give you a bar plot of the mean SHAP value magnitudes.
        bool_all_flavor_plot: False

        # as this takes much longer you can average the feature_sets to a
        # smaller set, 50 is a good choice for DL1r
        averaged_sets: 50

        # [11,11] works well for dl1r
        plot_size: [11, 11]
